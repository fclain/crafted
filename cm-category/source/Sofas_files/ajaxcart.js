jQuery.noConflict();
function pluscartAjax(info) {
    var qtyInput = jQuery(info).prev().find('.qty');
    var qty = parseInt(qtyInput.val()) + 1;

    /* data of current product */

    var _id = info.getAttribute('id');
    var id = info.getAttribute('product-id');
    var form_key = info.getAttribute('form-key');
    var base_url = info.getAttribute('base-url');
    var quote_id = info.getAttribute('quote-id');
    var discount = info.getAttribute('discount');

    /* change qty */

    qtyInput.val(qty);
    var maxadd = qtyInput.attr('maxadd');
    var maxstock = qtyInput.attr('maxstock');
    if (checkqtyaddcart(maxadd, maxstock, qty)) {
        jQuery('input.qty[quote-id="' + quote_id + '"]').removeClass('bg_red');
        jQuery('.item-msg').remove();
    } else {
        jQuery('.item-msg').remove();
        jQuery('input.qty[quote-id="' + quote_id + '"]').addClass('bg_red');
        jQuery('button[quote-id="' + quote_id + '"].js-increase-quantity').parent().after('<span class="item-msg error">' + Translator.translate('The requested quantity is not available.') + '</span>');

        jQuery('button[quote-id="' + quote_id + '"].js-increase-quantity').attr('disabled', 'disabled');
        return false;
    }
    ;
    /* post Array and Url */

    if (qty > 0) {
        var _url = base_url + 'checkout/cart/updatePost/?isAjax=1&no_cache=1'
        var post = 'form_key=' + form_key + '&update_cart_action=update_qty&cart[' + quote_id + '][qty]=' + qty;
    } else {
        var _url = base_url + "checkout/cart/delete/?isAjax=1&no_cache=1";
        var post = 'form_key=' + form_key + '&id=' + quote_id;
    }

    /* Delay 1 second */
    jQuery.doTimeout(_id, 1000, function () {
        _updatecart(id, _url, post, qty, quote_id, discount);
    });
    jQuery(".qty-box .btn-add-qty").unbind("click");
}
function minuscartAjax(info) {
    var qtyMinus = jQuery(info).next().find('.qty');
    if (parseInt(jQuery(info).next().find('.qty').val()) > 1) {
        var qty = parseInt(qtyMinus.val()) - 1;

    } else {
        var qty = 1;
    }
    /* data of current product */

    var _id = info.getAttribute('id');
    var id = info.getAttribute('product-id');
    var form_key = info.getAttribute('form-key');
    var base_url = info.getAttribute('base-url');
    var quote_id = info.getAttribute('quote-id');
    var discount = info.getAttribute('discount');

    /* change qty */

    qtyMinus.val(qty);
    var maxadd = qtyMinus.attr('maxadd');
    var maxstock = qtyMinus.attr('maxstock');
    if (checkqtyaddcart(maxadd, maxstock, qty)) {
        jQuery('input.qty[product-id="' + id + '"]').removeClass('bg_red');
        jQuery('.item-msg').remove();
    } else {
        jQuery('.item-msg').remove();
        jQuery('input.qty[quote-id="' + quote_id + '"]').addClass('bg_red');
        jQuery('button[quote-id="' + quote_id + '"].js-decrease-quantity').parent().after('<span class="item-msg error">' + Translator.translate('The requested quantity is not available.') + '</span>');
        return false;
    }
    ;
    jQuery('.col-main tr[product-id="' + id + '"] .radio').addClass('disabled');
    /* post Array and Url */

    if (qty > 0) {
        var _url = base_url + 'checkout/cart/updatePost/?isAjax=1&no_cache=1'
        var post = 'form_key=' + form_key + '&update_cart_action=update_qty&cart[' + quote_id + '][qty]=' + qty;
    } else {
        var _url = base_url + "checkout/cart/delete/?isAjax=1&no_cache=1";
        var post = 'form_key=' + form_key + '&id=' + quote_id;
    }

    /* Delay 1 second */

    jQuery.doTimeout(_id, 1000, function () {
        _updatecart(id, _url, post, qty, quote_id, discount);
    });

    jQuery(".qty-box .btn-remove-qty").unbind("click");
}
function _updatecart(id, _url, post, qty, quote_id, discount) {
    try {
        /* Turn on loading */
        jQuery('.line-item-quantity span.loading[quote-id="' + quote_id + '"]').show();
        /* Turn off button */
        jQuery('button[quote-id="' + quote_id + '"].btn-add-qty').attr('disabled', 'disabled');
        jQuery('button[quote-id="' + quote_id + '"].btn-remove-qty').attr('disabled', 'disabled');
        /* Turn off qty box */
        jQuery('input[product-id="' + id + '"] .qty').attr('disabled', 'disabled');
        jQuery.ajax({
            url: _url,
            type: 'post',
            data: post,
            dataType: 'json',
            success: function (data) {

                /*Turn off loading.*/
                jQuery('.line-item-quantity span.loading[quote-id="' + quote_id + '"]').hide();
                jQuery('#checkout-region .Cart-fixed .line-items .LineItem div[quote-id="' + quote_id + '"].prices span.price').html(data.cart[quote_id].base_row_total_incl_tax);
                jQuery('#checkout-region .Cart-fixed .order-summary .onestepcheckout-totals .subtotal td.value').html(data.subtotal);
                jQuery('#checkout-region .Cart-fixed .order-summary .onestepcheckout-totals .tax td.value').html(data.tax_info);
                jQuery('#checkout-region .Cart-fixed .order-summary .onestepcheckout-totals .grand_total td.value').html(data.grand_total);
                jQuery('#checkout-region .Cart-fixed .header-cart-fixed span.js-quantity').html(data.items_qty);
                jQuery('.header-cart span.js-keep-cart-btn .count-link').html(data.count_link);

                jQuery('input[product-id="' + id + '"] .qty').removeAttr("disabled");
                jQuery('button[product-id="' + id + '"].btn-add-qty').removeAttr("disabled");
                jQuery('button[product-id="' + id + '"].btn-remove-qty').removeAttr("disabled");

                if (qty == 0) {//if remove
                    jQuery('.button[product-id="' + id + '"]').remove();
                } else {
                    jQuery('td.product-cart-total[quote-id="' + quote_id + '"] .cart-price .price').html(data.cart[quote_id].price_discount);
                }
            }
        });
    } catch (e) {
    }
}
function checkqtyaddcart(maxadd, maxstock, qty) {
    if (maxstock == 0 && qty > maxadd) {
        return false;
    } else if (maxstock > 0 && qty > maxstock) {
        return false;
    } else if (maxstock > 0 && qty > maxadd) {
        return false;
    } else {
        return true;
    }
}
function updatecartQty(info) {
    /* data of current product */

    var id = info.getAttribute('product-id');
    var form_key = info.getAttribute('form-key');
    var base_url = info.getAttribute('base-url');
    var quote_id = info.getAttribute('quote-id');

    if (parseInt(info.value) > 0) {
        var qty = parseInt(info.value);
    } else {
        var qty = 1;
    }
    /*check change*/

    var change = false;
    if (parseInt(info.value) != parseInt(jQuery('input[product-id="' + id + '"] .qty').attr('value')) && parseInt(info.value) > 0) {
        change = true;
    }
    /*check qty*/

    var maxadd = info.getAttribute('maxadd');
    var maxstock = info.getAttribute('maxstock');
    if (checkqtyaddcart(maxadd, maxstock, qty)) {
        jQuery('input.qty[product-id="' + id + '"]').removeClass('bg_red');
        jQuery('.item-msg').remove();
    } else {
        jQuery('.item-msg').remove();
        jQuery('input.qty[quote-id="' + quote_id + '"]').addClass('bg_red');
        jQuery('button[quote-id="' + quote_id + '"].btn-add-qty').after('<span class="item-msg error">' + Translator.translate('The requested quantity is not available.') + '</span>');
        return false;
    }
    ;
    /* post Array and Url */

    if (qty > 0) {
        var _url = base_url + 'checkout/cart/updatePost/?isAjax=1&no_cache=1'
        var post = 'form_key=' + form_key + '&update_cart_action=update_qty&cart[' + quote_id + '][qty]=' + info.value;
    } else {
        qty = 1;
        var _url = base_url + "checkout/cart/delete/?isAjax=1&no_cache=1";
        var post = 'form_key=' + form_key + '&id=' + quote_id;
    }
    if (change) {
        _updatecart(id, _url, post, qty, quote_id);
    }

}

function pluscartAjaxLanding(info) {
    var qtyInput = jQuery(info).prev().find('.qty');
    var qty = parseInt(qtyInput.val()) + 1;

    /* data of current product */

    var _id = info.getAttribute('id');
    var id = info.getAttribute('product-id');
    var form_key = info.getAttribute('form-key');
    var base_url = info.getAttribute('base-url');
    var quote_id = info.getAttribute('quote-id');
    var discount = info.getAttribute('discount');

    /* change qty */

    qtyInput.val(qty);
    var maxadd = qtyInput.attr('maxadd');
    var maxstock = qtyInput.attr('maxstock');
    if (checkqtyaddcart(maxadd, maxstock, qty)) {
        jQuery('input.qty[quote-id="' + quote_id + '"]').removeClass('bg_red');
        jQuery('.item-msg').remove();
    } else {
        jQuery('.item-msg').remove();
        jQuery('input.qty[quote-id="' + quote_id + '"]').addClass('bg_red');
        jQuery('button[quote-id="' + quote_id + '"].js-increase-quantity').parent().after('<span class="item-msg error">' + Translator.translate('The requested quantity is not available.') + '</span>');

        jQuery('button[quote-id="' + quote_id + '"].js-increase-quantity').attr('disabled', 'disabled');
        return false;
    }
    ;
    /* post Array and Url */

    if (qty > 0) {
        var _url = base_url + 'checkout/cart/updatePost/?isAjax=1&no_cache=1'
        var post = 'form_key=' + form_key + '&update_cart_action=update_qty&cart[' + quote_id + '][qty]=' + qty;
    } else {
        var _url = base_url + "checkout/cart/delete/?isAjax=1&no_cache=1";
        var post = 'form_key=' + form_key + '&id=' + quote_id;
    }

    /* Delay 1 second */
    jQuery.doTimeout(_id, 1000, function () {
        _updatecartLanding(id, _url, post, qty, quote_id, discount);
    });
    jQuery(".qty-box .btn-add-qty").unbind("click");
}
function minuscartAjax(info) {
    var qtyMinus = jQuery(info).next().find('.qty');
    if (parseInt(jQuery(info).next().find('.qty').val()) > 1) {
        var qty = parseInt(qtyMinus.val()) - 1;

    } else {
        var qty = 1;
    }
    /* data of current product */

    var _id = info.getAttribute('id');
    var id = info.getAttribute('product-id');
    var form_key = info.getAttribute('form-key');
    var base_url = info.getAttribute('base-url');
    var quote_id = info.getAttribute('quote-id');
    var discount = info.getAttribute('discount');

    /* change qty */

    qtyMinus.val(qty);
    var maxadd = qtyMinus.attr('maxadd');
    var maxstock = qtyMinus.attr('maxstock');
    if (checkqtyaddcart(maxadd, maxstock, qty)) {
        jQuery('input.qty[product-id="' + id + '"]').removeClass('bg_red');
        jQuery('.item-msg').remove();
    } else {
        jQuery('.item-msg').remove();
        jQuery('input.qty[quote-id="' + quote_id + '"]').addClass('bg_red');
        jQuery('button[quote-id="' + quote_id + '"].js-decrease-quantity').parent().after('<span class="item-msg error">' + Translator.translate('The requested quantity is not available.') + '</span>');
        return false;
    }
    ;
    jQuery('.col-main tr[product-id="' + id + '"] .radio').addClass('disabled');
    /* post Array and Url */

    if (qty > 0) {
        var _url = base_url + 'checkout/cart/updatePost/?isAjax=1&no_cache=1'
        var post = 'form_key=' + form_key + '&update_cart_action=update_qty&cart[' + quote_id + '][qty]=' + qty;
    } else {
        var _url = base_url + "checkout/cart/delete/?isAjax=1&no_cache=1";
        var post = 'form_key=' + form_key + '&id=' + quote_id;
    }

    /* Delay 1 second */

    jQuery.doTimeout(_id, 1000, function () {
        _updatecartLanding(id, _url, post, qty, quote_id, discount);
    });

    jQuery(".qty-box .btn-remove-qty").unbind("click");
}
function _updatecartLanding(id, _url, post, qty, quote_id, discount) {
    try {
        /* Turn on loading */
        jQuery('.line-item-quantity span.loading[quote-id="' + quote_id + '"]').show();
        /* Turn off button */
        jQuery('button[quote-id="' + quote_id + '"].btn-add-qty').attr('disabled', 'disabled');
        jQuery('button[quote-id="' + quote_id + '"].btn-remove-qty').attr('disabled', 'disabled');
        /* Turn off qty box */
        jQuery('input[product-id="' + id + '"] .qty').attr('disabled', 'disabled');
        jQuery.ajax({
            url: _url,
            type: 'post',
            data: post,
            dataType: 'json',
            success: function (data) {
                location.reload();
            }
        });
    } catch (e) {
    }
}