jQuery(document).ready(function(){
    jQuery(".view-full-detail-btn").click(function(e){
        e.preventDefault();
        jQuery('html,body').animate({
                scrollTop: jQuery("#product-detail-area").offset().top},
            'slow');
    })
    var length = jQuery('#messages_product_view').find('.success-msg').length;
    if(length > 0){
        if(jQuery('#checkout-region').hasClass('active_minicart')){
            jQuery('#checkout-region').removeClass('active_minicart');
            jQuery('.window-cart-overlay').hide();
            jQuery('#checkout-region').animate({right: "-380"}, 500);
        }else{
            jQuery('#messages_product_view').find('.success-msg').css("display","none");
            jQuery('#checkout-region').addClass('active_minicart');
            jQuery('#checkout-region').animate({right: 0}, 500);
            jQuery('.window-cart-overlay').show();
        }
    }
    jQuery('.js-keep-cart-btn').click(function(e) {
        if(jQuery('#checkout-region').hasClass('active_minicart')){
            jQuery('#checkout-region').removeClass('active_minicart');
            jQuery('.window-cart-overlay').hide();
            jQuery('#checkout-region').animate({right: "-380"}, 500);
        }else{
            jQuery('#checkout-region').addClass('active_minicart');
            jQuery('#checkout-region').animate({right: 0}, 500);
            jQuery('.window-cart-overlay').show();
        }
    });
    jQuery('#header-special .header-special-close').click(function(){
        console.log('a');
        jQuery(this).parent().fadeOut(400);
    })
   // jQuery('.Cart-fixed .cart-body').css("margin-bottom", jQuery('.Cart-fixed footer').outerHeight());
    /* Back to top */
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() != 0) {
            jQuery('#bttop').fadeIn();
        } else {
            jQuery('#bttop').fadeOut();
        }
    });
    jQuery('#bttop').click(function () {
        jQuery('body,html').animate({scrollTop: 0}, 500);
    });
    jQuery(".catalog-category-view .category-products").addClass("add-position");
    jQuery(".catalog-category-view .category-products #machine-recs").addClass("position-absolute");
    scrollTopPos=0;
    var angle = 0;

    jQuery(window).on('scroll', function () {
        var diffPos = jQuery(this).scrollTop() - scrollTopPos ;


        scrollTopPos = jQuery(this).scrollTop();

        if (scrollTopPos > 120) {
            if(!jQuery("body").hasClass("animated")){
                jQuery("body").addClass("animated");
                jQuery(".catalog-category-view .category-products").removeClass("add-position");
                jQuery(".catalog-category-view .category-products #machine-recs").removeClass("position-absolute");
                jQuery("#header-topbar").hide(200);
                jQuery("#top-navigation").css("position", "fixed");
                jQuery("#top-navigation").animate({
                    width: "100%",
                    top: "0px"
                }, 200);
                jQuery("#top-navigation #gears").show(400);
                jQuery("#top-navigation .stickymenu").show(400);
                jQuery('#machine-recs').animate({
                    top: "75px"
                });
            }
        } else {
            if(jQuery("body").hasClass("animated")){
                jQuery("body").removeClass("animated");
                jQuery(".catalog-category-view .category-products").addClass("add-position");
                jQuery(".catalog-category-view .category-products #machine-recs").addClass("position-absolute");
                jQuery("#header-topbar").show(400);
                jQuery("#top-navigation").css("position", "relative");
                jQuery("#top-navigation").animate({
                    width: "auto"
                }, 400);
                jQuery("#top-navigation #gears").hide(400);
                jQuery("#top-navigation .stickymenu").hide(400);
                if(jQuery("body").hasClass("catalog-category-view")){
                    jQuery('#machine-recs').animate({
                        top: "66px"
                    });
                }else{
                    jQuery('#machine-recs').animate({
                        top: "195px"
                    });
                }

            }
        }


    });

// Get the container position
    var containerPos = jQuery('body').offset();

    // Get the initial scroll position. This will be needed later when determining
    // if we are scrolling up or down.
    var scrollPos = jQuery(window).scrollTop();
    var degreeRotate = 0;

    // We will use these to track how much we are rotating our gears. Need to track
    // the gears separately since they will not be going the same direction
    var gear1Rotate = 0;
    var gear2Rotate = 0;
    var gear3Rotate = 0;

    jQuery('#top-navigation #gears').css('display', 'none');
    jQuery('#top-navigation #gears').css('left', containerPos.left + "px");
    browser_transform('#top-navigation #gear2', 11);
    browser_transform('#top-navigation #gear3', 90);

    jQuery(document).scroll(function() {
        // Are we moving up or down?
        var newScroll = jQuery(window).scrollTop();

        if (scrollPos > newScroll) {
            degreeRotate -= 15;
        } else {
            degreeRotate += 15;
        }

        // Calculate rotations. These will be slightly different for each gear, even
        // for the ones spinning the same direction, in order to line up the teeth of
        // the gears.
        gear1Rotate = degreeRotate;
        gear2Rotate = ((degreeRotate + 11) * -1);
        gear3Rotate = ((degreeRotate + 90) * -1);

        // Store the current scroll for comparison next scroll event.
        scrollPos = newScroll;

        browser_transform('#top-navigation #gear1', gear1Rotate);
        browser_transform('#top-navigation #gear2', gear2Rotate);
        browser_transform('#top-navigation #gear3', gear3Rotate);
    });


    /*var scrollTopPos=0;
     var angle = 0;

     jQuery(window).on('scroll', function () {
     var diffPos = jQuery(this).scrollTop() - scrollTopPos ;

     console.log(diffPos);

     scrollTopPos = jQuery(this).scrollTop();

     if (scrollTopPos > 100) {
     if(!jQuery("body").hasClass("animated")){
     jQuery("body").addClass("animated");
     jQuery("#header-topbar").hide(200);
     jQuery("#top-navigation").css("position", "fixed");
     jQuery("#top-navigation").animate({
     width: "100%",
     top: "0px"
     }, 200);
     jQuery("#top-navigation .stickymenuimg").show(400);
     jQuery("#top-navigation .stickymenu").show(400);
     }
     if(diffPos > 0){

     setTimeout(function(){
     angle+=30;
     jQuery("#top-navigation #image").rotate(angle);
     },50);

     }else{
     setTimeout(function(){
     angle+=-30;
     jQuery("#top-navigation #image").rotate(angle);
     },50);
     }
     } else {
     if(jQuery("body").hasClass("animated")){
     jQuery("body").removeClass("animated");
     jQuery("#header-topbar").show(400);
     jQuery("#top-navigation").css("position", "relative");
     jQuery("#top-navigation").animate({
     width: "auto"
     }, 400);
     jQuery("#top-navigation .stickymenuimg").hide(400);
     jQuery("#top-navigation .stickymenu").hide(400);
     }
     }

     });*/

    jQuery( "#draggable3" ).draggable({ containment: "#containment-wrapper", scroll: false });
    /*contacts page*/
    jQuery("a.new-order").click(function(){
        jQuery("#landing").hide();
        jQuery("#new-sales").fadeIn();
    });
    jQuery("a.other").click(function(){
        jQuery("#landing").hide();
        jQuery("#new-sales").fadeIn();
    });
    jQuery("a.existing-customer-register").click(function(){
        jQuery("#landing").hide();
        jQuery("#exsiting-customer").fadeIn();
    });
    jQuery("a.send-email-existing-account").click(function(){
        jQuery("#form-contact-existing-account").fadeIn();
    });
    /*new-sale - send email*/
    jQuery("#new-sales .send-email").click(function(){
        jQuery("#send-email-sales").fadeIn();
    });
    jQuery("#contactpopup-close").click(function(){
        jQuery("#form-contact-existing-account").fadeOut();
    });
    /*edit delivery code*/
    jQuery(".estimate-shipping-text a").click(function(){
       jQuery("#homeqshippingEstimate_sidebar").hide();
        jQuery("#formbox").show();
    });

    /*hover how we do*/

    /*jQuery(".how-we-do .how-we-do-content .columns").mouseover(function(){
        jQuery(this).append("<div class='img-hover'>&nbsp;</div>");
    }).mouseout(function(){
        jQuery(this).find(".img-hover").remove();
    });*/
});

jQuery(window).resize(function(){
    /*position for back to cart when resize height*/
    var height = jQuery(window).height();
    if(height<400) {
        jQuery('.sign-in-component').css('padding-top','10px');

    }
    else {
        jQuery('.sign-in-component').css('padding-top','6rem');
    }
});
function browser_transform(transTarget, transValue)
{
    jQuery(transTarget).css('-ms-transform', 'rotate(' + transValue + 'deg)');
    jQuery(transTarget).css('-moz-transform', 'rotate(' + transValue + 'deg)');
    jQuery(transTarget).css('-webkit-transform', 'rotate(' + transValue + 'deg)');
    jQuery(transTarget).css('-o-transform', 'rotate(' + transValue + 'deg)');
    jQuery(transTarget).css('transform', 'rotate(' + transValue + 'deg)');
}

function expandView(hrefElement)
{
    var element = jQuery(hrefElement);
    var spanElement = element.prev().children("span:last");

    if(spanElement.hasClass('hidden'))
    {
        spanElement.removeClass('hidden');
        element.html(Translator.translate('View Less'));
    }
    else {
        spanElement.addClass('hidden');
        element.html(Translator.translate('View More'));
    }
}
                            