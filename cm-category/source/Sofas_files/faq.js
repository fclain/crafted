jQuery(document).ready(function(){
	if(jQuery('.mw_faq').length > 0){
		jQuery('.mw_faq').each(function(){
			var faqOneBlock = jQuery(this);
			faqOneBlock.find('.mw_question').click(function(){
				if(jQuery(this).next('.mw_answer').is(':hidden')){
					faqOneBlock.find('.mw_question').removeClass('mw_question_a_selected');
					faqOneBlock.find('.mw_answer').slideUp('fast');
					
					jQuery(this).addClass('mw_question_a_selected');
					jQuery(this).next('.mw_answer').slideDown('fast');
				}
			});
		});
	}
});