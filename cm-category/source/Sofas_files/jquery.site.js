jQuery.noConflict();

//jQuery(window).on('load', );
jQuery(window).load(function(){
	initOpenClose();
	calculateHomeFeaturesPadding();
});
jQuery(document).ready(function(){
	/*if(jQuery('.product-essential').length > 0){
	    var x = jQuery('.product-essential .medium-5').offset().left;
	    var stop = jQuery('.product-description').offset().top-250;
	    var start = jQuery('.product-essential .medium-5').offset().top;
	    jQuery(window).scroll(function () {
	        var y = jQuery(window).scrollTop();
	        if(y > start && y < stop)
	            jQuery('.product-essential .medium-5').css({'top': '50px', 'left': x+'px','position': 'fixed'});
	        else{
	            if(y >= stop)
	                jQuery('.product-essential .medium-5').css({'top': stop+'px', 'left': x+'px','position': 'absolute'});
	            else{
	                if(y < start)
	                    jQuery('.product-essential .medium-5').css({'top': start+'px', 'left': x+'px','position': 'absolute'});
	            }
	        }
	    });
    }*/

	setInterval('asSeenInRotate()', 6000);
	jQuery("#navigation li.level0").hover(function(){
		if(jQuery(this).children('.drop-holder').length > 0){
			jQuery(this).addClass('has-drop');
		}
		jQuery(this).addClass('hover').children('.drop-holder').show();
	}, function(){
		if(jQuery(this).children('.drop-holder').length > 0){
			jQuery(this).removeClass('has-drop');
		}
		jQuery(this).removeClass('hover').children('.drop-holder').hide();
	});
	
	var bannerProduct = jQuery('#top-navigation .topnav li .drop-holder .drop .product-banner');
	if(bannerProduct.length) {
		bannerProduct.each(function() {
			var self = jQuery(this),
				productRow = self.siblings('.product-row');
			if(productRow.length) {
				productRow.addClass('product-row-with-banner');
			}
		});
	}

	if(jQuery().jcarousel) {
		carouselProducts();
		carouselProductsSingleView();
	}
	
	jQuery('.filter-link').click(function(){
		jQuery('.products-topbar').toggle();
		return false;
	});
	filterNavigation();
	jQuery(window).resize(function(){
		calculateHomeFeaturesPadding();
		filterNavigation();	
	});
});

function gettotalajax(object, url){
    jQuery('.waiting-loading').show();
    object.prop('disabled', true);
    jQuery.ajax({
        type:  "POST",
        url:   url,
        dataType:   "json",
        success: function(result){
            jQuery('#shopping-cart-totals').html(result.html);
            object.prop('disabled', false);
            jQuery('.waiting-loading').hide();
        }
    });
}

function asSeenInRotate(){
	var widthWindow = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	if(widthWindow > 928){
		var $active = jQuery('#as-seen-in-logos .active');
	    var $next = ($active.next().length > 0) ? $active.next() : jQuery('#as-seen-in-logos span img:first');
	    $active.animate({opacity: 0},500,function(){
			$active.removeClass('active');
	    	$next.animate({opacity: 1},650).addClass('active');
	    });
    }
}

function carouselProducts(){
	if(jQuery('.jcarousel').length > 0){
		jQuery('.jcarousel')
			.on('jcarousel:create jcarousel:reload', function() {
		        var element = jQuery(this),
		            width = element.innerWidth();
	
	                if (width >= 900) {
	                    width = width / 4;
	                } else if (width >= 640) {
	                    width = width / 3;
	                } else if (width >= 436) {
	                    width = width / 2;
	                } else{
		                width = width;
	                }
	                
		        element.jcarousel('items').css('width', width + 'px');
		    })
	        .jcarousel({
	        	wrap: 'circular'
	        });
		
		jQuery('.jcarousel-control-prev')
	        .jcarouselControl({
	        	target: '-=1'
	        });
	
	    jQuery('.jcarousel-control-next').jcarouselControl({
	        	target: '+=1'
	        });
	}
}

function carouselProductsSingleView(){
	if(jQuery('.jcarousel-single').length > 0){
		jQuery('.jcarousel-single')
			.on('jcarousel:create jcarousel:reload', function() {
		        var element = jQuery(this),
		            width = element.innerWidth();
		                            
		        element.jcarousel('items').css('width', width + 'px');
		    })
		    .jcarousel({
	    		wrap: 'circular'
	    	});
		
		jQuery('.jcarousel-control-single-prev')
	        .jcarouselControl({
	        	target: '-=1'
	        });
	
	    jQuery('.jcarousel-control-single-next').jcarouselControl({
	        	target: '+=1'
	        });
	}
}

function calculateHomeFeaturesPadding(){
	if(jQuery('.home-features').length > 0){
		var widthWindow = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		jQuery('.home-features li').each(function(){
			var contentHeight = jQuery(this).find('.details').height(),
				imgHeight = jQuery(this).find('.image img').height();
			
			var marginResult = ((imgHeight - contentHeight)/2).toFixed(0);
			if(marginResult > 1 && widthWindow > 577){
				jQuery(this).find('.details').css('margin-top', (marginResult/16)+'rem');
			} else{
				jQuery(this).find('.details').removeAttr('style');
			}
		});
	}
}

function initOpenClose(){
    var navbox = jQuery('.header-cart'),
        cart = '.header-cart-count',
        dropSelector = '.cartdrop',
        slideCss = {
            display: 'none'
        };

    navbox.on('mouseenter', navbox, function(){
        var slide = navbox.find(dropSelector);
        slide.css(slideCss);
        slide.css('display', 'block');
    });

    navbox.on('mouseleave', navbox, function(){
        var slide = navbox.find(dropSelector);
        slide.css('display', 'none');
    });
}

function filterNavigation(){
if(jQuery('.layered-nav').length > 0){
	var widthWindow = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	if(widthWindow > 768){
		jQuery('.products-topbar').show();
		jQuery('.layered-nav .item').removeClass('active');
		jQuery('.item-drop').hide();
		
		jQuery('.layered-nav .item').each(function(){
			jQuery(this).hover(function(){
				jQuery(this).addClass('active');
				jQuery(this).find('.item-drop').show();
			}, function(){
				jQuery(this).removeClass('active');
				jQuery(this).find('.item-drop').hide();
			});
		});
	} else{
		jQuery('.layered-nav .item').each(function(){
			var itemLayer = jQuery(this);
			itemLayer.unbind('mouseenter mouseleave');
			itemLayer.find('.item-title').click(function(){
				jQuery('.layered-nav .item').removeClass('active');
				jQuery('.item-drop').hide();
				
				itemLayer.addClass('active');
				itemLayer.find('.item-drop').show();
			});
		});
	}
}
}