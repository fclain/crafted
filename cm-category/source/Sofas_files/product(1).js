function homeqshippingGetShippingRates(){

    /*if($('homeqshippingLoading').style.display != "none")
    {
        return false;
    }
    */
    postCode = $('homeqshippingPostcode');
    postCode.value = postCode.value.replace(/[^0-9\.]/g,'');
    if(jQuery(postCode).val().length != 4){
        return false;
    }
    $('homeqshippingLoading').show();
    $('homeqshippingEstimate').hide();
    $('homeqshippingEstimateError').hide();


    var super_attribute = {};
    if ($$('[name^=super_attribute]').length > 0) {
        $$('[name^=super_attribute]').each(function(item){
            var re = new RegExp(['super_attribute\\[', '\\]'].join("|"), "gi");
            var attrId = item.name.replace(re, '');
            super_attribute[attrId] = item.value;
        });
    }

    var options = {};
    $$('select[name^=super_attribute]').each(function (control) {
        if (false === options) {
            return;
        }
        if (!control.value) {
            options = false;
            return;
        } else {
            options[control.name.substr(16, control.name.length - 17)] = control.value;
        }
    });
    /*
    if (false == options) {
        alert('Please select product options');
        return false;
    }
    */
    var options_str = '';
    for (var i in options) {
        options_str += i + ':' + options[i] + ';';
    }
    options = {};
    $$('select[name^=options]').each(function (control) {
        if (false === options) {
            return;
        }
        if (control.hasClassName('required-entry')) {
            if (!control.value) {
                options = false;
                return;
            } else {
                options[control.name.substr(8, control.name.length - 9)] = control.value;
            }
        }
    });
    $$('input[name^=options]').each(function (control) {
        if (false === options) {
            return;
        }
        if (control.hasClassName('validate-one-required-by-name')) {
            var items = $$('input[name^=options]');
            var value = false;
            for (var k = 0; k < items.length; k++) {
                if ((items[k].name == control.name) && items[k].checked) {
                    value = items[k].value;
                }
            }
            if (!value) {
                options = false;
                return;
            } else {
                options[control.name.substr(8, control.name.length - 9)] = value;
            }
        }
    });
    if (false == options) {
        alert('Please select product options');
        return false;
    }
    var pr_options_str = '';
    for (var i in options) {
        pr_options_str += i + ':' + options[i] + ';';
    }

    //bundle products
    options = {};
    $$('select[name^=bundle_option]').each(function (control) {
        if (false === options) {
            return;
        }
        if (control.hasClassName('required-entry')) {
            if (!control.value) {
                options = false;
                return;
            } else {
                options[control.name.substr(14, control.name.length - 15)] = control.value;
            }
        }
    });
    $$('input[name^=bundle_option]').each(function (control) {
        if (false === options) {
            return;
        }
        if (control.hasClassName('validate-one-required-by-name')) {
            var items = $$('input[name^=bundle_option]');
            var value = false;
            for (var k = 0; k < items.length; k++) {
                if ((items[k].name == control.name) && items[k].checked) {
                    value = items[k].value;
                }
            }
            if (!value) {
                options = false;
                return;
            } else {
                options[control.name.substr(13, control.name.length - 14)] = value;
            }
        }
    });
    if (false == options) {
        alert('Please select product options');
        return false;
    }
    var bundle_options_str = '';
    for (var i in options) {
        bundle_options_str += i + ':' + options[i] + ';';
    }

    //get rid of '[' & ']' - for check-boxes with syntax xxx[14][]
    var pattern = /(\[|\])/g;
    bundle_options_str = bundle_options_str.replace(pattern, "");



    new Ajax.Request('customshipping/estimate/run', {
        parameters: {
            postcode  : $('homeqshippingPostcode').value,
            product_id: $('homeqshippingProduct_id').value,
            super_attribute: JSON.stringify(super_attribute),
            qty       : $('homeqshippingQty').value,
            options   : options_str,
            pr_options: pr_options_str,
            bl_options: bundle_options_str
        },
        onComplete: function(resp){
            resp = resp.responseText.evalJSON();
            $('homeqshippingLoading').hide();
            if (resp.success == 1) {
                $('homeqshippingEstimateError').hide();
                $('homeqshippingEstimate').show();
                $('homeqshippingEstimatePrice').update(resp.price);
            } else {
                $('homeqshippingEstimateError').show();
            }
        },
        method: 'post'
    });
}

function homeqshippingGetShippingRatesCartSidebar(){

    postCode = $('homeqshippingPostcode_sidebar');
    postCode.value = postCode.value.replace(/[^0-9\.]/g,'');
    if(jQuery(postCode).val().length != 4){
        return false;
    }

    $('homeqshippingLoading_sidebar').show();
    $('homeqshippingEstimate_sidebar').hide();
    $('homeqshippingEstimateError_sidebar').hide();
    var super_attribute = {};
    if ($$('[name^=super_attribute]').length > 0) {
        $$('[name^=super_attribute]').each(function(item){
            var re = new RegExp(['super_attribute\\[', '\\]'].join("|"), "gi");
            var attrId = item.name.replace(re, '');
            super_attribute[attrId] = item.value;
        });
    }
    new Ajax.Request('/customshipping/estimate/quote', {
        parameters: {
            postcode  : $('homeqshippingPostcode_sidebar').value,
            //product_id: $('homeqshippingProduct_id_sidebar').value,
            //super_attribute: JSON.stringify(super_attribute),
            //qty       : $('homeqshippingQty_sidebar').value
        },
        onComplete: function(resp){
            resp = resp.responseText.evalJSON();
            $('homeqshippingLoading_sidebar').hide();
            if (resp.success == 1) {
                $('homeqshippingEstimateError_sidebar').hide();
                $('homeqshippingEstimate_sidebar').show();
                $('formbox').hide();
                $('homeqshippingEstimatePrice_sidebar_code').innerHTML = $('homeqshippingPostcode_sidebar').value;
                $('homeqshippingEstimatePrice_sidebar').update(resp.price);
                $('cartsidebar-grandtotal').update(resp.grand_total);

            } else {
                $('homeqshippingEstimateError_sidebar').show();
            }
        },
        method: 'post'
    });
}
